python3 -m venv venv

source venv/bin/activate

python -m pip install --upgrade pip

pip install openweathermapy
pip install requests
pip install numpy
pip install pymap3d
pip install pandas
pip install matplotlib
pip install scipy
pip install lmfit
pip install bezier
pip install gekko
pip install plotext
pip install fitparse
pip install python-time
pip install seaborn
pip install PyYAML
pip install argparse
pip install plotly
pip install asyncio