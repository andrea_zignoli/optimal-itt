# Optimal-ITT: optimal control applied to the sport of road cycling

There are two main scripts in this repository, and they try both to solve the dynamics of cycling. In a first case, the inverse dynamics is solved, and a Python script is used to estimate the power output delivered by the cyclist during a ride. In a second case, the predictive dynamics problem, is solved to compute the optimal maneouver and hence create an audible feedback to the rider. The two cases are presented together in a single repository, as the first original idea was to have a single script managing both the inverse and predictive dynamics. At the time of writing, however, scripts are still spearated and the problems are solved in isolation. They are presented here in two categories: 1) the inverse duynamics problem solver (power output estimator) and 2) the predictive dynamics problem solver (Curve warning system - Advanced Rider Assistance System (ARAS)).

### Power output estimator

The script environment.py has been conceived to estimate power output and other variables that cannot be accessed by direct measurements (e.g. roll angle, lateral accelerations, etc.) durign cycling road races and training sessions. You can run this analysis by starting with a fit files or data collected with a mobile app (see details below in the 'Data provided' section).

### Curve warning system - Advanced Rider Assistance System (ARAS)

The script warning_system.py has been conceived to provide audible feedback to a cyclist involved in a fast descending section. 

## Installation on PC

This project has been devloped in Python language. Use the package manager [pip](https://pip.pypa.io/en/stable/) to install project requirements. Everything is already specified in the setup_PC bash file. 

```bash
chmod +x setup_PC.sh
./setup_PC.sh
```

## Installation on Android device

On Android device, download an install [Termux](https://termux.com/). We suggest opening an ssh connection with your device and then log from remote. 

```bash
pkg install openssh
sshd
```

For some reasons, at the time of writing, some package installation via pip is highly problematic within a venv e.g. for scipy and numpy with Termux. Unfortunately, we did not manage to use venv on mobile. For the mobile device the package management system pip comes with [Python](https://wiki.termux.com/wiki/Python) and everything is already specified in the setup_mobile bash file. Please notice that a venv is not used. 

```bash
pkg install python
pkg install git
git clone https://andrea_zignoli@bitbucket.org/andrea_zignoli/optimal-itt.git
chmod +x setup_mobile.sh
./setup_mobile.sh
```

## Open weather API

An APIID should be requested to [Openweather](https://openweathermap.org/), and should therefore included in the configuration file to enable real-time wind velocity and direction from GPS data. You shuold remove our APIID from the config file and use yours accordingly.

## Data provided

### ARAS

The map of the course used to calibrate, test and develop the ARAS is fully available from [this map](https://www.google.com/maps/d/u/0/edit?mid=1sPurswAcnyHrcBGoYTEUvt3c5dViJNhP&usp=sharing). Follwing this course, the [Physics Toolbox Sensor Suite](https://play.google.com/store/apps/details?id=com.chrystianvieyra.physicstoolboxsuite&hl=en&gl=US) mobile app was used to collect the data included in the from_phone.csv file. Two additional files have been provided. GPS-data-mapped-kmz.txt was obtained from the Google Map service directly selecting the starting point and the ending point (a kml file was then exported and used in the altitude estimation with the [GPS visualiser](https://www.gpsvisualizer.com/) tool). The GPS-data-tracked.txt was obtained with the Google Earth service by manually selcting all the midline points (a kml file was then exported and used in the altitude estimation with the [GPS visualiser](https://www.gpsvisualizer.com/) tool).  

### Power output estimator

The file giro_2018.fit is provided to use the power output estimator. 

## Config file

A yaml config file is used to change user's preferences. Please edit the config.yml file on your local machine to enable/disable options. 

```yaml
# YAML file

plot:
    False
audio:
    False
openweather:
    settings:
        active: no
        APPID: -----> CHANGE HERE YOUR APIID
        units: metric
    default: 
        speed: 0
        deg: -90
fit_file:
    path: ../data/
    name: giro_2018
kml_file:
    path: ../data/
    name: Route
athlete:
    m: 78
    h: 1.83
    name: ''
simulation:
    # inverse
    predictive
GPS_points:
    first_point: 0
    last_point: 2000
    proportion_nodes_points: 4
    n_points: 12
    stride: 6
    margin_ahead: 4
```

# Usage

Run either the warning_system.py file or the environment.py file (after activating your venv - on PC)

```bash
source venv/bin/activate
python src/warning_system.py
```

or 

```bash
source venv/bin/activate
python src/environment.py
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change/improve.

## Equation of motion

The bike rider model inlcudes a number of differential equations. The reader is invited to read the 'Additional reading' papers (links below) for definition of the symbols and of the parameters. However, equations and symbols can also be found in the corresponding Python scripts, i.e.: environment.py and curve_warning.py.

The equation of motion for the heading angle is:

![alpha](https://latex.codecogs.com/svg.latex?\frac{d}{ds}\alpha(s)=\frac{\delta_n(s)\delta_{max}}{L\dot{s}(s)}-\kappa(s)) 

The equation of motion for the lateral displacement is:

![n](https://latex.codecogs.com/svg.latex?\frac{d}{ds}n(s)=\frac{1}{\dot{s}(s)}\left(v(s)\sin(\alpha(s))\right)) 

The equation of motion for the roll angle is:

![phi](https://latex.codecogs.com/svg.latex?\frac{d}{ds}\phi(s)=\frac{\dot{\phi}(s)}{\dot{s}(s)})

The equation of motion for the normalized steering angle is:

![delta_n](https://latex.codecogs.com/svg.latex?\frac{d}{ds}\delta_n(s)=\frac{\dot{\delta}_n(s)}{\dot{s}(s)})

The equation of motion for the time is:

![time](https://latex.codecogs.com/svg.latex?\frac{d}{ds}t(s)=-\frac{1+\kappa(s)n(s)}{\cos(\alpha(s))v(s)})

The equation of motion for the time-derivative of the curvilinear abscissa is:

![s_dot](https://latex.codecogs.com/svg.latex?\frac{d}{ds}\dot{s}(s)=\frac{v(s)\cos(\alpha(s))}{1-n(s)\kappa(s)})

The equation of motion for the time-derivative of the roll angle is:

![phi_dot](https://latex.codecogs.com/svg.latex?\frac{d}{ds}\dot{\phi}(s)=\frac{hmg}{I_XgL\dot{s}(s)}(v(s)^2\delta_{max}\delta_n(s)+Lg\phi(s)))

The equation of motion for the normalized power output is:

![W_n](https://latex.codecogs.com/svg.latex?\frac{d}{ds}W_n(s)=\frac{\dot{W}_n(s)}{\dot{s}(s)})

The equation of motion for the longitudinal speed is:

![v](https://latex.codecogs.com/svg.latex?\frac{mv(s)}{W_{max}}\frac{d}{ds}v(s)=\frac{W_n(s)}{\dot{s}(s)}-\frac{v(s)}{\dot{s}(s)W_{max}}(mg[C_{rr}\cos(\beta(s))+\sin(\beta(s))]+k_v(v(s)-V_w(\alpha(s)))^2))

## Additional reading

In the following papers, reference values and definitions of all parameters and variables are provided. 

[Prediction of pacing and cornering strategies during cycling individual time trials with optimal control](https://link.springer.com/article/10.1007/s12283-020-00326-x)

[Influence of corners and road conditions on cycling individual time trial performance and ‘optimal’pacing strategy: A simulation study](https://journals.sagepub.com/doi/abs/10.1177/1754337120974872)

## Cost function

### ARAS

![J](https://quicklatex.com/cache3/5d/ql_3a19f92f342d31d11898e8053b7a535d_l3.png)

### Power output estimator

![J1](https://quicklatex.com/cache3/86/ql_9a545c1858cc5d298e8acded4d176686_l3.png)

![J2](https://quicklatex.com/cache3/fa/ql_5c181356f3f453ef6405a1b68579a3fa_l3.png)

![J3](https://quicklatex.com/cache3/39/ql_d15b5f58cb57588e96a3637f3e75b439_l3.png)

![J](https://quicklatex.com/cache3/83/ql_f68fede2ba9069d71e680d20fc323883_l3.png)

## Problem details

### ARAS

These details have been retrieved from the solver output:

```bash
This is Ipopt version 3.12.10, running with linear solver ma57.
Number of nonzeros in equality constraint Jacobian...:     4286
Number of nonzeros in inequality constraint Jacobian.:      280
Number of nonzeros in Lagrangian Hessian.............:     1390
Total number of variables............................:     1546
                     variables with only lower bounds:      525
                variables with lower and upper bounds:      248
                     variables with only upper bounds:        0
Total number of equality constraints.................:     1295
Total number of inequality constraints...............:      140
        inequality constraints with only lower bounds:      140
   inequality constraints with lower and upper bounds:        0
        inequality constraints with only upper bounds:        0
```

## License

[MIT](https://choosealicense.com/licenses/mit/)