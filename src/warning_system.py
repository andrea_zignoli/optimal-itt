#!../venv/lib python

import utilities
import sys
import openweathermapy.core as owm
import requests
import numpy as np
import pymap3d as pm
import pandas as pd
from scipy.optimize import curve_fit
from scipy.integrate import odeint
import math
from scipy.optimize import minimize
from scipy.optimize import least_squares
from lmfit import Model
from scipy.optimize import Bounds
import bezier
from gekko import GEKKO
from scipy.interpolate import interp1d
import scipy.spatial.transform
from scipy import interpolate
import plotext
from fitparse import FitFile
import time as time_count
import yaml
import json
import os
import argparse
from pykml import parser as prs
from scipy.signal import savgol_filter

parser = argparse.ArgumentParser()
parser.add_argument("--device", help="running fromm PC or mobile")
args = parser.parse_args()

if args.device == 'PC' or len(sys.argv) < 2:
    print("Running from PC")
    import matplotlib
    import matplotlib.pyplot as plt
    import plotly.graph_objects as go

    text_params = {'ha': 'center',
                   'va': 'center',
                   'family': 'sans-serif',
                   'fontweight': 'bold',
                   'size': 8}

    font = {'family': 'Helvetica',
            'weight': 'normal',
            'size': 12}

    matplotlib.rc('font', **font)

if args.device == 'mobile':
    print("Running from mobile device")

sys.path.append('../venv/lib')

# set the current directory here
os.chdir(sys.path[0])

# load configuration file
a_yaml_file = open("config.yml")
parsed_yaml_file = yaml.load(a_yaml_file,
                             Loader=yaml.FullLoader)

# default predictive dynamics
predictive = True

# define a objective function for fitting exp data
def objective(x, a, b, c, d, e, f):
    return a * x ** 5 + b * x ** 4 + c * x ** 3 + d * x ** 2 + e * x + f

# load kml file converted in txt from GPS visualiser
# this is the data that you download BEFORE you can use the APP
GPS_data = pd.read_csv('../data/GPS-data-mapped-kmz.txt', delimiter='\t', skiprows=0)

# load the kml tracked from google Earth (just for comparison)
# load kml file converted in txt from GPS visualiser
GPS_data_map = pd.read_csv('../data/GPS-data-tracked.txt', delimiter='\t', skiprows=0)

# this is the data that you download REAL-TIME when you use the app -> from phone
real_time_data = pd.read_csv('../data/from_phone.csv')
real_time_data = real_time_data[real_time_data['Speed (m/s)'] > 3]
real_time_data = real_time_data.drop_duplicates('Latitude')
real_time_data = real_time_data.drop_duplicates('Longitude')
real_time_data = real_time_data[np.diff(real_time_data['time'], prepend=0) < 2]

# compute the closest point
path_lat0, path_lon0 = GPS_data['latitude'].values[0], GPS_data['longitude'].values[0]
RT_lat, RT_lon = real_time_data['Latitude'], real_time_data['Longitude']

# athlete data
m0 = parsed_yaml_file['athlete']['m']
h0 = parsed_yaml_file['athlete']['h']
W__max0 = 14 * m0
# C__D0 = 4.45 * (m0**(-0.45))
C__D0 = 0.75
# A__f0 = 0.0293 * (h0 ** 0.725 * m0 ** 0.425)
A__f0 = 0.35

# import fit file data and create a pd dataframe
Watts = []
lat = []
lon = []
alt = []
dist = []
Speed = []
time_stamps = []
time = []

exp_data = pd.DataFrame()
exp_data['lon'] = GPS_data['longitude']
exp_data['lat'] = GPS_data['latitude']
exp_data['alt'] = savgol_filter(GPS_data['altitude (m)'], 19, 1)

exp_data_map = pd.DataFrame()
exp_data_map['lon'] = GPS_data_map['longitude']
exp_data_map['lat'] = GPS_data_map['latitude']
exp_data_map['alt'] = GPS_data_map['altitude (m)']
 
first_point = parsed_yaml_file['GPS_points']['first_point']
last_point = parsed_yaml_file['GPS_points']['last_point']
# initial conditions for unknown variables
dist_tot_travelled = np.asarray(0)

proportion_nodes_points = parsed_yaml_file['GPS_points']['proportion_nodes_points']
n_points = parsed_yaml_file['GPS_points']['n_points']
stride = parsed_yaml_file['GPS_points']['stride']
n_nodes = n_points * proportion_nodes_points
# this margin has been introduced because s>1 in some cases
margin_ahead = parsed_yaml_file['GPS_points']['margin_ahead']
initial_distance = 0
time_tot_passed = 0
xCOM_current = 0
yCOM_current = 0
xEXP_current = 0
yEXP_current = 0

# adjust GPS
rotMatrix = scipy.spatial.transform.Rotation.from_euler('z', 0, degrees=True).as_matrix()

# IC
alpha0 = 0
d_alpha0 = 0
n0 = 0
phi__dot0 = 0
phi0 = 0
delta__n0 = 0
W__n0 = 0
v__delta_n0 = 0
v__W_n0 = 0
adj_CdA0 = 1
adj_beta0 = 1
ax0 = 0
ay0 = 0

# post process
if (args.device == 'PC' or len(sys.argv) < 2) and parsed_yaml_file['plot']:
    # initialize plot on PC
    # fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
    # plotly
    fig = go.Figure()

# AUDIO feedback
if parsed_yaml_file['audio']:
    import pyttsx3 as pyttsx
    from subprocess import call
    engine = pyttsx.init()
    engine.setProperty('rate', 180)
    voices = engine.getProperty('voices')
    voice = voices[0].id
    engine.setProperty('voice', voice)

import asyncio
def background(f):
    def wrapped(*args, **kwargs):
        return asyncio.get_event_loop().run_in_executor(None, f, *args, **kwargs)
    return wrapped

# set initial computational time
compt_time = time_count.time()
# set initial distance
progressive_segment = list()
progressive_distance = list()
progressive_velocity = list()
progressive_cycling_velocity = list()
progressive_pitch = list()
last_distance = 0
distance_stride = list([0])
progressive_beta = list()
progressive_power_output = list()
progressive_kappa_bezier = list()
progressive_kappa_bike = list()
progressive_kappa_RT = list()
progressive_ax = list()
progressive_ay = list()

distance = list()
power = list()
speed = list()
alpha_sim = list()
n_sim = list()
CPU_time = list()
converged = list()

GPS_route_points = np.empty([0, 3])
GPS_route_geo_coords = exp_data[['lat', 'lon', 'alt']].values[:]
GPS_route_points = np.array(pm.geodetic2enu(GPS_route_geo_coords[:, 0],
                                             GPS_route_geo_coords[:, 1],
                                             GPS_route_geo_coords[:, 2],
                                             GPS_route_geo_coords[0, 0],
                                             GPS_route_geo_coords[0, 1],
                                             GPS_route_geo_coords[0, 2]))

for i in np.arange(first_point, last_point, stride):

    try:
        # compute the first GPS-route point (index of)
        min_d = np.Inf
        route_GPS_idx0 = 0
        for j_, (lat_, lon_) in enumerate(zip(exp_data['lat'][1:], exp_data['lon'][1:])):
            d = np.sqrt((real_time_data['Latitude'].values[i] - lat_) ** 2 +
                        (real_time_data['Longitude'].values[i] - lon_) ** 2)
            if d < min_d:
                min_d = d
                route_GPS_idx0 = j_
                lat_first_point = lat_
                lon_first_point = lon_
            else:
                pass

        if route_GPS_idx0 > 0:

            # first GPS point mapped GPS
            lat0, lon0, alt0 = exp_data[['lat', 'lon', 'alt']].iloc[route_GPS_idx0]

            # compute the first GPS-route point (index of) with TRACKED map
            min_d = np.Inf
            route_GPS_idx0_map = 0
            for j_, (lat_, lon_) in enumerate(zip(exp_data_map['lat'][1:], exp_data_map['lon'][1:])):
                d = np.sqrt((real_time_data['Latitude'].values[i] - lat_) ** 2 +
                            (real_time_data['Longitude'].values[i] - lon_) ** 2)
                if d < min_d:
                    min_d = d
                    route_GPS_idx0_map = j_
                    lat_first_point = lat_
                    lon_first_point = lon_
                else:
                    pass

            if route_GPS_idx0_map == -1:
                route_GPS_idx0_map = 0

            lat0_map, lon0_map, alt0_map = exp_data_map[['lat', 'lon', 'alt']].iloc[route_GPS_idx0_map]

            # wind estimation
            location = (lat0, lon0)
            if parsed_yaml_file['openweather']['settings']['active']:
                data_wind = owm.get_current(location, **parsed_yaml_file['openweather']['settings'])
            else:
                data_wind = dict()
                data_wind['wind'] = parsed_yaml_file['openweather']['default']

            Vw0__0 = data_wind['wind']['speed']
            wD__0 = data_wind['wind']['deg']/180 * math.pi

            trajectory_points = np.empty([0, 3])
            geo_coords = exp_data[['lat', 'lon', 'alt']].values[route_GPS_idx0:route_GPS_idx0+n_points+margin_ahead]
            trajectory_points = np.array(pm.geodetic2enu(geo_coords[:, 0],
                                                         geo_coords[:, 1],
                                                         geo_coords[:, 2],
                                                         lat0, lon0, alt0))

            # interpolate nominal trajectory (GPS visualizer)
            enu = rotMatrix.dot(trajectory_points)

            # ENU coordinates
            x = enu[0, :]
            y = enu[1, :]
            z = enu[2, :]

            # Altitude used to compute road slope beta
            alt_exp = np.asarray(exp_data['alt'].values[route_GPS_idx0:route_GPS_idx0+n_points + margin_ahead])
            alt_exp_F = savgol_filter(alt_exp, 5, 1)
            dist_exp = np.sqrt((x[:-1] - x[1:]) ** 2 + (y[:-1] - y[1:]) ** 2)
            dist_exp_tot = np.sum(dist_exp)
            beta_exp_1 = np.arctan(np.diff(alt_exp_F) / dist_exp)
            beta_exp = savgol_filter(beta_exp_1, 5, 1)

            # find the point where the RT data gets closest to the GPS route you downloaded
            # this is used to establish when you need to start the OC calculation,
            # i.e. when you overtake the first available GPS point
            RT_points = np.empty([0, 3])
            RT_geo_coords = real_time_data[['Latitude', 'Longitude']].values[i:i+n_points + margin_ahead + 10]
            RT_points = np.array(pm.geodetic2enu(RT_geo_coords[:, 0],
                                                 RT_geo_coords[:, 1],
                                                 np.zeros(np.size(RT_geo_coords[:, 1])),
                                                 lat0,
                                                 lon0,
                                                 alt0))

            # ENU coordinates
            x_RT = RT_points[0, :]
            y_RT = RT_points[1, :]
            z_RT = RT_points[2, :]

            # here is the function that computes kappa and theta and other important trajectory definition
            # MIDLINE
            kappa_bezier, theta_bezier, curve1, s_bezier, dist_tot, s_inter = utilities.compute_kappa(x, y, n_nodes, margin_ahead, proportion_nodes_points)

            # CYCLING TRAJECTORY
            kappa_RT, theta_RT, curve2, s_RT, dist_tot_RT, s_inter_RT = utilities.compute_kappa(x_RT, y_RT, n_nodes,
                                                                                                      margin_ahead,
                                                                                                      proportion_nodes_points)

            # iterate to find the minimal distance and determine the starting point
            min_d = np.Inf
            for s_ in s_bezier:
                xB_ = curve1.evaluate_multi(np.asfortranarray(s_))[0]
                yB_ = curve1.evaluate_multi(np.asfortranarray(s_))[1]
                d = np.sqrt((RT_points[0, 0] - xB_) ** 2 + (RT_points[1, 0] - yB_) ** 2)
                if d < min_d:
                    # this might be a surrogate for n0
                    min_d = d
                    # this might be a surrogate for s0
                    s_start = s_
                else:
                    pass

            # time and speed
            time_exp = np.asarray(real_time_data['time'].values[i:i + n_points + margin_ahead])
            v_exp = np.asarray(real_time_data['Speed (m/s)'].values[i:i + n_points + margin_ahead])
            dist_exp_actual = v_exp[1:] * np.diff(time_exp)

            # CREATE FUNCTIONS THAT YOU CAN USE IN GEKKO
            pitch_exp = np.asarray(real_time_data['Pitch'].values[i:i + n_points + margin_ahead]/180*np.pi)
            pitch_steps = interp1d(np.concatenate(([0], np.cumsum(dist_exp_actual))), pitch_exp,
                                   fill_value="extrapolate",
                                 kind='linear')
            pitch = lambda x: pitch_steps(x)

            if s_start * dist_tot <= dist_exp_actual[0]:
                vel_steps = interp1d(np.concatenate(([s_start * dist_tot], np.cumsum(dist_exp_actual))), v_exp,
                                     fill_value="extrapolate", kind='linear')
            else:
                vel_steps = interp1d(np.cumsum(np.concatenate((dist_exp_actual, [np.mean(dist_exp_actual)]))), v_exp,
                                     fill_value="extrapolate", kind='linear')

            vel = lambda x: vel_steps(x)

            # popt_beta, _ = curve_fit(objective, np.concatenate(([0], np.cumsum(dist_exp[:-1]))), beta_exp)
            # beta = lambda x: popt_beta[0] * x ** 5 + popt_beta[1] * x ** 4 + popt_beta[2] * x ** 3 + popt_beta[3] * x**2 + popt_beta[4] * x + popt_beta[5]
            beta_steps = interp1d(np.concatenate(([s_start * dist_tot], np.cumsum(dist_exp[:-1]))), beta_exp,
                                  fill_value="extrapolate", kind='cubic')
            beta = lambda x: beta_steps(x)

            # MIDLINE
            popt_k, _ = curve_fit(objective, s_inter[:-2], kappa_bezier)
            kappa = lambda x: popt_k[0] * x ** 5 + popt_k[1] * x ** 4 + popt_k[2] * x ** 3 + popt_k[3] * x**2 + popt_k[4] * x + popt_k[5]
            # kappa_steps = interp1d(s_inter[:-2], kappa_bezier, fill_value="extrapolate", kind='cubic')
            # kappa = lambda x: kappa_steps(x)

            # CYCLING TRAJECTORY
            popt_k_RT, _ = curve_fit(objective, s_inter_RT[:-2], kappa_RT)
            kappa_RT_fnc = lambda x: popt_k_RT[0] * x ** 5 + popt_k_RT[1] * x ** 4 + popt_k_RT[2] * x ** 3 + popt_k_RT[3] * x ** 2 + \
                              popt_k_RT[4] * x + popt_k_RT[5]

            # popt_theta, _ = curve_fit(objective, s_inter[:-1], theta_bezier)
            # theta_fun = lambda x: popt_theta[0] * x ** 5 + popt_theta[1] * x ** 4 + popt_theta[2] * x ** 3 + popt_theta[3] * x**2 + popt_theta[4] * x + popt_theta[5]
            theta_steps = interp1d(s_inter[:-1], theta_bezier, fill_value="extrapolate", kind='linear')
            theta_fun = lambda x: theta_steps(x)

            # INITIALISE GEKKO
            model = GEKKO(remote=True)
            model.DIAGLEVEL = 0
            # maximal CPU time (s)
            # Do not go beyond the number of points, i.e. the stride.
            model.MAX_TIME = n_points

            # refresh ICs
            # initial lateral displacement
            n0 = - min(4, min_d[0])
            # initial abscissa
            s0 = s_start * dist_tot
            # initial speed (experimental speed)
            v0 = real_time_data['Speed (m/s)'].values[i]
            # initial longitudinal acceleration (experimental)
            ax0 = np.diff(real_time_data['Speed (m/s)'].values[i:i+2])
            # intial heading (set to 0)
            alpha0 = 0

            # STATE VARIABLES (check papers for nomenclature)
            # heading
            alpha = model.Var(lb=-0.4, ub=0.4, value=alpha0)
            # time-variation curvilinear abscissa
            s__dot = model.Var()
            # longitudinal speed
            v = model.Var(lb=1, value=v0)
            # RIGHT LANE ONLY
            # lateral displacement
            n = model.Var(lb=-5, ub=0, value=n0)
            # time-derivative roll angle
            phi__dot = model.Var(value=phi__dot0)
            # roll angle
            phi = model.Var(value=phi0, lb=-0.7, ub=0.7)
            # normalised steering angle
            delta__n = model.Var(value=delta__n0)
            # normalised power output
            W__n = model.Var(value=W__n0)
            # this first great difference with previous version
            # curvilinear abscissa
            s = model.Var(value=s0)
            # time
            t_var = model.Var(value=0)
            # heading time-derivative
            d_alpha = model.Var(value=d_alpha0)
            # longitudinal acc
            ax = model.Var(lb=-9.8 * 0.8, ub=9.8 * 0.8, value=ax0)
            # lateral acc
            ay = model.Var(lb=-9.8 * 0.8, ub=9.8 * 0.8)

            # Parameters
            delta__max = 0.78
            L = 1.4
            k = 0
            d = 0.25
            W__max = W__max0
            I__X = 1
            h = 1
            g = 9.81
            C__rr = 0.0065
            C__D = C__D0
            A__f = A__f0
            rho = 1.23
            m = m0
            theta = 0
            mu__max = 0.7
            Cb__0 = 0
            Cb__1 = 0
            Vw0 = Vw0__0
            wD = wD__0
            adj_CdA = 1
            adj_beta = 1
            tau = .1

            # time-invariant variables
            v_target = model.Param()
            beta_target = model.Param()

            # CONTROLS
            v__delta_n = model.MV(value=v__delta_n0, lb=-0.75, ub=0.75)
            v__delta_n.STATUS = 1
            v__W_n = model.MV(value=v__W_n0, lb=-0.1, ub=0.01)
            v__W_n.STATUS = 1

            # ROAD & ENVIRONMENT
            Vw = lambda alpha, theta, Vw0, wD: Vw0 * model.cos(alpha + theta - wD)
            Vw_np = lambda alpha, theta, Vw0, wD: Vw0 * np.cos(alpha + theta - wD)

            # math utilities
            neg_part = lambda x: x + (0.001 + 0.5 * (((-x) - 0.001) - model.sqrt(((-x) - 0.001) ** 2 + 0.001 ** 2)))
            pos_part = lambda x: x - (0.001 + 0.5 * (((x) - 0.001) - model.sqrt(((x) - 0.001) ** 2 + 0.001 ** 2)))
            neg_part_np = lambda x: x + (0.001 + 0.5 * (((-x) - 0.001) - np.sqrt(((-x) - 0.001) ** 2 + 0.001 ** 2)))
            pos_part_np = lambda x: x - (0.001 + 0.5 * (((x) - 0.001) - np.sqrt(((x) - 0.001) ** 2 + 0.001 ** 2)))

            # Constraints
            model.Equation(delta__n >= -0.75)
            model.Equation(delta__n <= 0.75)
            model.Equation(W__n >= -1)
            model.Equation(W__n <= 1)
            model.Equation(phi > -0.6)
            model.Equation(phi < 0.6)
            model.Equation(v > 3)
            # RIGHT LANE ONLY
            model.Equation(n > -4.5)
            model.Equation(n < 0)
            model.Equation(t_var >= 0)
            # Friction ellipse ax ay mu__max and g
            model.Equation((ax/(mu__max * g))**2 + (ay/(mu__max * g))**2 < 1)

            # set up optimiser
            model.options.IMODE = 6
            model.options.SOLVER = 3
            model.options.MAX_ITER = 1200
            model.MAX_TIME = stride
            model.options.rtol = 1e-3
            model.options.otol = 1e-3
            # model.options.REDUCE = 3
            # model.solver_options = ['linear_solver mumps', 'mu_strategy adaptive']

            # time -> space GEKKO notation
            model.time = s_inter

            # target values of velocity
            v_target.value = vel(s_inter)
            # if target velocity os met THEN you can trust cubic approx of beta
            beta_target.value = beta(s_inter)

            # ICs: free initial vars
            model.free_initial(delta__n)
            model.free_initial(phi)
            model.free_initial(ay)
            model.free_initial(d_alpha)
            model.free_initial(alpha)
            model.free_initial(phi__dot)
            model.free_initial(W__n)
            # ICs: fix initial vars (Biral et al. 2010)
            model.fix_initial(v, val=v0)
            model.fix_initial(n, val=n0)
            model.fix_initial(s, val=s0)
            model.fix_initial(t_var, val=0)
            # model.fix_initial(ax, val=ax0)
            # free some finals
            model.free_final(n) # Biral et al. fixed this to 0
            model.free_final(delta__n)
            model.free_final(phi)
            model.free_final(W__n)
            model.free_final(v)
            model.free_final(phi__dot)
            model.free_final(d_alpha)
            model.free_final(ax)
            model.free_final(alpha)

            # FREE some controls at the beginning
            model.free_initial(v__delta_n)
            # FIX some controls at the beginning
            model.free_initial(v__W_n)
            # FREE some controls at the end
            model.free_final(v__delta_n)
            model.free_final(v__W_n)

            # Equations
            # 1) alpha heading angle
            model.Equation(alpha.dt() == -0.1e1 * (n * kappa(s) * delta__n * delta__max + model.cos(alpha) * kappa(
                s) * L - 0.1e1 * delta__max * delta__n) / L / model.cos(alpha))
            # 2) lateral displacement
            model.Equation(n.dt() == -0.1e1 * model.sin(alpha) / model.cos(alpha) * (-0.1e1 + kappa(s) * n))
            # 3) roll angle
            model.Equation(phi.dt() == -0.1e1 * phi__dot / model.cos(alpha) / v * (-0.1e1 + kappa(s) * n))
            # 4) norm steering angle
            model.Equation(delta__n.dt() == -0.1e1 * v__delta_n / model.cos(alpha) / v * (-0.1e1 + kappa(s) * n))
            # 5) norm power
            model.Equation(W__n.dt() == -0.1e1 * v__W_n / model.cos(alpha) / v * (-0.1e1 + kappa(s) * n))
            # 8) roll angle rate
            model.Equation(phi__dot.dt() == 0.5 / v ** 2 / L / (h ** 2 * m + I__X) * h / model.cos(alpha) * (
                        C__D * A__f * Vw(alpha, theta, Vw0, wD) ** 2 * adj_CdA * v * n * kappa(s) * rho * delta__n * d * delta__max
                        - 0.2e1 * C__D * A__f * Vw(alpha, theta, Vw0, wD) * adj_CdA * v ** 2 * n * kappa(
                    s) * rho * delta__n * d * delta__max
                        + C__D * A__f * adj_CdA * v ** 3 * n * kappa(s) * rho * delta__n * d * delta__max
                        - 0.1e1 * C__D * A__f * Vw(alpha, theta, Vw0, wD) ** 2 * adj_CdA * v * rho * delta__n * d * delta__max
                        + 0.2e1 * C__D * A__f * Vw(alpha, theta, Vw0, wD) * adj_CdA * v ** 2 * rho * delta__n * d * delta__max
                        - 0.1e1 * C__D * A__f * adj_CdA * v ** 3 * rho * delta__n * d * delta__max
                        - 0.2e1 * v * n * kappa(s) * neg_part(W__n) * g * m * mu__max * delta__n * d * delta__max
                        + 0.2e1 * v * n * kappa(s) * model.cos(adj_beta * beta_target) * C__rr * g * m * delta__n * d * delta__max
                        + 0.2e1 * v * n * kappa(s) * model.sin(adj_beta * beta_target) * g * m * delta__n * d * delta__max +
                        0.2e1 * v * neg_part(W__n) * g * m * mu__max * delta__n * d * delta__max -
                        0.2e1 * v * model.cos(adj_beta * beta_target) * g * m * C__rr * delta__n * d * delta__max
                        - 0.2e1 * v ** 3 * delta__n * n * kappa(s) * m * delta__max -
                        0.2e1 * v ** 2 * n * kappa(s) * Cb__1 * delta__n * d * delta__max -
                        0.2e1 * v ** 2 * v__delta_n * n * kappa(s) * d * m * delta__max -
                        0.2e1 * L * phi * v * n * kappa(s) * g * m -
                        0.2e1 * v * model.sin(adj_beta * beta_target) * g * m * delta__n * d * delta__max -
                        0.2e1 * v * kappa(s) * pos_part(W__n) * W__max * delta__n * d * delta__max +
                        0.2e1 * v * n * kappa(s) * Cb__0 * delta__n * d * delta__max +
                        0.2e1 * v ** 3 * delta__n * m * delta__max + 0.2e1 * v ** 2 * Cb__1 * delta__n * d * delta__max +
                        0.2e1 * v ** 2 * v__delta_n * d * m * delta__max + 0.2e1 * L * phi * v * g * m +
                        0.2e1 * pos_part(
                    W__n) * W__max * delta__n * d * delta__max - 0.2e1 * v * Cb__0 * delta__n * d * delta__max))
            # 9) longitudinal speed
            model.Equation(v.dt() == 0.5000000000e0 * (C__D * A__f * Vw(alpha, theta, Vw0, wD) ** 2 * adj_CdA * v * n * kappa(s) * rho -
                                                       0.2e1 * C__D * A__f * Vw(alpha, theta, Vw0, wD) * adj_CdA * v ** 2 * n * kappa(
                        s) * rho +
                                                       C__D * A__f * adj_CdA * v ** 3 * n * kappa(s) * rho -
                                                       0.1e1 * C__D * A__f * Vw(alpha, theta, Vw0, wD) ** 2 * adj_CdA * v * rho +
                                                       0.2e1 * C__D * A__f * Vw(alpha, theta, Vw0, wD) * adj_CdA * v ** 2 * rho -
                                                       0.1e1 * C__D * A__f * adj_CdA * v ** 3 * rho -
                                                       0.2e1 * v * n * kappa(s) * neg_part(W__n) * g * m * mu__max +
                                                       0.2e1 * v * n * kappa(s) * model.cos(
                        adj_beta * beta_target) * C__rr * g * m +
                                                       0.2e1 * v * n * kappa(s) * model.sin(adj_beta * beta_target) * g * m +
                                                       0.2e1 * v * neg_part(W__n) * g * m * mu__max -
                                                       0.2e1 * v * model.cos(adj_beta * beta_target) * g * m * C__rr -
                                                       0.2e1 * v ** 2 * n * kappa(s) * Cb__1 -
                                                       0.2e1 * v * model.sin(adj_beta * beta_target) * g * m -
                                                       0.2e1 * v * kappa(s) * pos_part(W__n) * W__max +
                                                       0.2e1 * v * n * kappa(s) * Cb__0 +
                                                       0.2e1 * v ** 2 * Cb__1 +
                                                       0.2e1 * pos_part(W__n) * W__max -
                                                       0.2e1 * v * Cb__0) / m / v ** 2 / model.cos(alpha))
            # 10) time
            model.Equation(t_var.dt() == -0.1e1 / model.cos(alpha) / v * (-0.1e1 + kappa(s) * n))
            # 11) space
            model.Equation(s.dt() == 1)
            # 12) heading
            model.Equation(alpha.dt() == d_alpha)
            # 13) ay
            model.Equation(ay.dt() == (- ay + (v**2 * delta__max * delta__n/L))/tau)
            # 14) ax
            model.Equation(ax.dt() == ((v * neg_part(W__n) * g * m * mu__max - v * model.cos(beta_target) * g * m * C__rr -
                                       v * model.sin(beta_target) * g * m + v ** 2 * Cb__1 -
                                       0.5e0 * v * (v - Vw(alpha, theta, Vw0, wD)) ** 2 * (1 + pos_part(beta_target)) * C__D0 * A__f0 * rho -
                                       v * Cb__0 + W__max * pos_part(W__n)) / m / v - ax)/tau)

            # Objective function COEFFICIENTS
            W__J1 = model.Param(value=10)
            W__J2 = model.Param(value=1)
            W__J3 = model.Param(value=1)
            W__J4 = model.Param(value=0.01)
            W__J5 = model.Param(value=0.01)
            W__J6 = model.Param(value=0.1)
            Jf = model.FV()  # final time objective
            Jf.STATUS = 1
            model.Connection(Jf, t_var, pos2='end')
            # Objective function
            model.Obj((W__J1 * (v__delta_n ** 2) +
                       W__J2 * (v__W_n ** 2) +
                       W__J3 * (W__n ** 2) +
                       W__J4 * Jf +
                       W__J5 * (phi ** 2) +
                       W__J6 * (d_alpha ** 2)
                       ))

            # launch solver
            model.solve(disp=True, debug=0)

            # save converged/not converged
            if model.options.APPINFO == 0:
                converged.append(0)
            else:
                converged.append(1)

            CPUtime_ = model.options.SOLVETIME
            print('Solution time:', CPUtime_, ' s')

            CPU_time.append(CPUtime_)

            # you cannot compute the last point on simulation vars, but on exp vars
            last_distance = np.cumsum(dist_exp)[- (n_points - stride)]
            # last_distance = s_inter[-(n_points - stride - 1)]
            distance_stride.append(last_distance)

            # POST PROCESS
            # progressive COM position
            s_norm = np.asfortranarray(np.asarray(s.value)[s.value < last_distance] / last_distance)
            s_value = np.asfortranarray(np.asarray(s.value)[s.value < last_distance])
            alpha_value = np.asarray(alpha.value)[s.value < last_distance]
            n_value = np.asarray(n.value)[s.value < last_distance]

            xCOM = curve1.evaluate_multi(s_norm)[0, :] - np.sin(
                theta_fun(s_value) + alpha_value) * n_value
            yCOM = curve1.evaluate_multi(s_norm)[1, :] + np.cos(
                theta_fun(s_value) + alpha_value) * n_value

            x_midline = curve1.evaluate_multi(s_norm)[0, :]
            y_midline = curve1.evaluate_multi(s_norm)[1, :]

            x_L_edge = curve1.evaluate_multi(s_norm)[0, :] - np.sin(
                theta_fun(s_value)) * 4
            y_L_edge = curve1.evaluate_multi(s_norm)[1, :] + np.cos(
                theta_fun(s_value)) * 4
            x_R_edge = curve1.evaluate_multi(s_norm)[0, :] + np.sin(
                theta_fun(s_value)) * 4
            y_R_edge = curve1.evaluate_multi(s_norm)[1, :] - np.cos(
                theta_fun(s_value)) * 4

            # DETECT TURNS AND PROVIDE AUDIO FEEDBACK
            if parsed_yaml_file['audio']:
                @background
                def audible_feedback(i):
                    phrase = 'Fast turn on the right in ' + str(i) + ' meters'
                    call(["python3", "audible_feedback.py", phrase])

                # Provide audio feedback
                audible_feedback(i)

            # VISUAL INSPECTION ON MOBILE DEVICE
            if args.device == 'mobile' and parsed_yaml_file['plot']:
                plotext.clp()
                plotext.clt()
                plotext.scatter(xCOM_current + xCOM, yCOM_current + yCOM, point_color = "tomato")
                plotext.scatter(xCOM_current + x, yCOM_current + y, point_color = "green")
                plotext.show()

            # plot service on PC
            if (args.device == 'PC' or len(sys.argv) < 2) and parsed_yaml_file['plot']:
                from matplotlib import cm

                # if i == first_point:
                    # TODO: adjust plot issues.
                up = True
                axs1 = plt.subplot(321)
                axs2 = plt.subplot(122)
                axs1.axvline(x=0, c='lightgrey')

                axs1.axvline(x=np.cumsum(distance_stride)[-1], c='lightgrey')
                axs1.text(np.cumsum(distance_stride)[-1] - distance_stride[-1]/2, 70, str(len(distance_stride)-1),
                            bbox={'boxstyle': 'circle', 'color': 'whitesmoke', 'ec': 'black'}, **text_params)

                if up:
                    axs1.plot((np.concatenate(([0], np.cumsum(dist_exp))) + np.cumsum(distance_stride)[-2])[
                                    np.concatenate(([0], np.cumsum(dist_exp))) + np.cumsum(distance_stride)[-2] <
                                    np.cumsum(distance_stride)[-1]],
                                v_exp[np.concatenate(([0], np.cumsum(dist_exp))) + np.cumsum(distance_stride)[-2] <
                                      np.cumsum(distance_stride)[-1]] * 3.6, c='darkorange', linestyle='--', linewidth=2)
                    axs1.plot(np.asarray(s.value) - s[0] +
                                np.cumsum(distance_stride)[-2],
                                np.asarray(v.value) * 3.6, c='orangered', linewidth=2)
                    up = False
                else:
                    axs1.plot((np.concatenate(([0], np.cumsum(dist_exp))) + np.cumsum(distance_stride)[-2])[
                                    np.concatenate(([0], np.cumsum(dist_exp))) + np.cumsum(distance_stride)[-2] <
                                    np.cumsum(distance_stride)[-1]],
                                v_exp[np.concatenate(([0], np.cumsum(dist_exp))) + np.cumsum(distance_stride)[-2] <
                                      np.cumsum(distance_stride)[-1]] * 3.6, c='dodgerblue', linestyle='--', linewidth=2)
                    axs1.plot(np.asarray(s.value) - s[0] +
                                np.cumsum(distance_stride)[-2],
                                np.asarray(v.value) * 3.6, c='mediumblue', linewidth=2)
                    up = True

                axs1.set_xlabel('Distance (m)')
                axs1.set_ylabel('Speed (km/h)')

                if up:
                    axs2.plot(np.array(x_L_edge + GPS_route_points[0, route_GPS_idx0]),
                             np.array(y_L_edge + GPS_route_points[1, route_GPS_idx0]), 'tab:gray')
                    axs2.plot(np.array(x_R_edge + GPS_route_points[0, route_GPS_idx0]),
                              np.array(y_R_edge + GPS_route_points[1, route_GPS_idx0]), 'tab:gray')
                    axs2.plot(np.array(x_midline + GPS_route_points[0, route_GPS_idx0]),
                              np.array(y_midline + GPS_route_points[1, route_GPS_idx0]), 'tab:gray', linestyle='--')
                    axs2.plot(np.array(xCOM + GPS_route_points[0, route_GPS_idx0]),
                              np.array(yCOM + GPS_route_points[1, route_GPS_idx0]), c='tab:blue')
                    axs2.text(np.average(np.array(x_L_edge + GPS_route_points[0, route_GPS_idx0])),
                              np.average(np.array(y_L_edge + GPS_route_points[1, route_GPS_idx0])),
                              str(len(distance_stride) - 1),
                              bbox={'boxstyle': 'circle', 'color': 'whitesmoke', 'ec': 'black'}, **text_params)
                else:
                    axs2.plot(np.array(x_L_edge + GPS_route_points[0, route_GPS_idx0]),
                              np.array(y_L_edge + GPS_route_points[1, route_GPS_idx0]), c='tab:red')
                    axs2.plot(np.array(x_R_edge + GPS_route_points[0, route_GPS_idx0]),
                              np.array(y_R_edge + GPS_route_points[1, route_GPS_idx0]), c='tab:red')
                    axs2.plot(np.array(x_midline + GPS_route_points[0, route_GPS_idx0]),
                              np.array(y_midline + GPS_route_points[1, route_GPS_idx0]), 'tab:red', linestyle='--')
                    axs2.plot(np.array(xCOM + GPS_route_points[0, route_GPS_idx0]),
                              np.array(yCOM + GPS_route_points[1, route_GPS_idx0]), c='tab:green')
                    axs2.text(np.average(np.array(x_L_edge + GPS_route_points[0, route_GPS_idx0])),
                              np.average(np.array(y_L_edge + GPS_route_points[1, route_GPS_idx0])),
                              str(len(distance_stride) - 1),
                              bbox={'boxstyle': 'circle', 'color': 'whitesmoke', 'ec': 'black'}, **text_params)

                # axs[1].plot(np.array(x_new_F + GPS_route_points[0, route_GPS_idx0]),
                #          np.array(y_new_F + GPS_route_points[1, route_GPS_idx0]), 'xr')
                # axs[1].plot(np.array(xCOM[0] + GPS_route_points[0, route_GPS_idx0]),
                #          np.array(yCOM[0] + GPS_route_points[1, route_GPS_idx0]), 'ob')
                # axs[1].plot(RT_points[0, :] + GPS_route_points[0, route_GPS_idx0],
                #          RT_points[1, :] + GPS_route_points[1, route_GPS_idx0], 'xb')
                axs2.axis('equal')
                # axs2.set_ylabel('coherence')
                axs2.spines['top'].set_visible(False)
                axs2.spines['bottom'].set_visible(False)
                axs2.spines['right'].set_visible(False)
                axs2.spines['left'].set_visible(False)
                axs2.tick_params(
                    axis='y',  # changes apply to the x-axis
                    which='both',  # both major and minor ticks are affected
                    left=False,  # ticks along the bottom edge are off
                    labelleft=False)  # labels along the bottom edge are off
                axs2.tick_params(
                    axis='x',  # changes apply to the x-axis
                    which='both',  # both major and minor ticks are affected
                    bottom=False,  # ticks along the bottom edge are off
                    top=False,  # ticks along the top edge are off
                    labelbottom=False)  # labels along the bottom edge are off
                axs2.spines['top'].set_visible(False)
                axs2.spines['right'].set_visible(False)
                # axs[1].grid(True)

                dummy = 1

            # post processing vars
            progressive_segment.extend(list(np.ones(len(np.asarray(s.value[:stride * proportion_nodes_points]) + initial_distance)) * i))
            progressive_distance.extend(list(np.asarray(s.value[:stride * proportion_nodes_points]) + initial_distance))
            progressive_velocity.extend(list(np.asarray(v.value[:stride * proportion_nodes_points]) * 3.6))
            progressive_cycling_velocity.extend(list(vel(np.asarray(s.value[:stride * proportion_nodes_points])) * 3.6))
            progressive_beta.extend(list(beta(np.asarray(s.value[:stride * proportion_nodes_points]))))
            progressive_pitch.extend(list(pitch(np.asarray(s.value[:stride * proportion_nodes_points]))))
            progressive_power_output.extend(list(np.asarray(W__n[:stride * proportion_nodes_points]) * W__max))
            # compute trajectory curvature (1/R)
            kappa_exp = np.diff(np.asarray(alpha)) / np.diff(np.asarray(s))
            progressive_kappa_bike.extend(np.asarray(d_alpha[:stride * proportion_nodes_points])/np.diff(np.asarray(s))[0])
            progressive_kappa_bezier.extend(list(kappa(np.asarray(s.value[:stride * proportion_nodes_points]))))
            progressive_kappa_RT.extend(list(kappa_RT_fnc(np.asarray(s.value[:stride * proportion_nodes_points]))))
            progressive_ax.extend(list(np.asarray(ax[:stride * proportion_nodes_points]) / g))
            progressive_ay.extend(list(np.asarray(ay[:stride * proportion_nodes_points]) / g))
            # update initial progressive distance
            initial_distance = progressive_distance[-1]

        else:
            print('Not quite there yet')
            pass
    except:
        ('Out of the track')


df = pd.DataFrame()
df['distance'] = progressive_distance
df['OC_speed'] = progressive_cycling_velocity
df['speed'] = progressive_velocity
df['ax'] = progressive_ax
df['ay'] = progressive_ay
df['kappa_bike'] = progressive_kappa_bike
df['kappa_bezier'] = progressive_kappa_bezier
df['kappa_RT'] = progressive_kappa_RT
df['segment'] = progressive_segment
df['pitch'] = progressive_pitch
df.to_csv('../output/results.csv')

foo=0
fig = plt.gcf()
fig.set_size_inches(18, 6)
plt.tight_layout()
plt.show()
df_results = pd.DataFrame()

df_results['time'] = np.concatenate(sim_time, axis=0)
df_results['speed'] = np.concatenate(speed, axis=0)
df_results['power'] = np.concatenate(power, axis=0)
df_results['alpha'] = np.concatenate(alpha_sim, axis=0)
df_results['n'] = np.concatenate(n_sim, axis=0)
df_results['target_power'] = np.concatenate(target_power, axis=0)
df_results['target_speed'] = np.concatenate(target_speed, axis=0)
df_results['abs_power'] = df_results['power']
df_results['abs_power'][df_results.abs_power < 0] = 0

# drop duplicates (due to overlapping)
df_results = df_results.drop_duplicates('time')
df_results = df_results[np.diff(df_results['time'], prepend=0) > 0]
# add row of zeros
df0 = pd.DataFrame([[0]*df_results.shape[1]], columns=df_results.columns)
df_results = df0.append(df_results, ignore_index=True)

# create time series
df_results['time'] = pd.to_datetime(df_results["time"], unit = 's')
df_results = df_results.set_index('time')

if args.device == 'PC' and parsed_yaml_file['plot']:
    fig, (ax1) = plt.subplots(1, 1)
    ax1.scatter(30, np.max(df_results.rolling('30s').abs_power.mean()), facecolor='blue')
    ax1.scatter(120, np.max(df_results.rolling('120s').abs_power.mean()), facecolor='blue')
    ax1.scatter(300, np.max(df_results.rolling('300s').abs_power.mean()), facecolor='blue')
    ax1.scatter(600, np.max(df_results.rolling('600s').abs_power.mean()), facecolor='blue')
    ax1.scatter(30, np.max(df_results.rolling('30s').target_power.mean()), facecolor='red')
    ax1.scatter(120, np.max(df_results.rolling('120s').target_power.mean()), facecolor='red')
    ax1.scatter(300, np.max(df_results.rolling('300s').target_power.mean()), facecolor='red')
    ax1.scatter(600, np.max(df_results.rolling('600s').target_power.mean()), facecolor='red')

# save a dict with the results
df_results = df_results.reset_index()
df_results['time'] = df_results['time'].astype(str)
dict_results = df_results.to_dict()
dict_results['CPU_time'] = CPU_time

# write json
with open('../output/' + parsed_yaml_file['fit_file']['name'] + '_results.json', 'w') as outfile:
    json.dump(dict_results, outfile)

EOF = 1
# EOF