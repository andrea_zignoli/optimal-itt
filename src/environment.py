#!../venv/lib python

import sys
import openweathermapy.core as owm
import requests
import numpy as np
import pymap3d as pm
import pandas as pd
from scipy.optimize import curve_fit
from scipy.integrate import odeint
import math
from scipy.optimize import minimize
from scipy.optimize import least_squares
from lmfit import Model
from scipy.optimize import Bounds
import bezier
from gekko import GEKKO
from scipy.interpolate import interp1d
import scipy.spatial.transform
from scipy import interpolate
import plotext
from fitparse import FitFile
import time as time_count
import yaml
import json
import os
import argparse
from pykml import parser as prs

parser = argparse.ArgumentParser()
parser.add_argument("--device", help="running from PC or mobile")
args = parser.parse_args()

if args.device == 'PC' or len(sys.argv) < 2:
    print("Running from PC")
    import matplotlib.pyplot as plt

if args.device == 'mobile':
    print("Running from mobile device")

sys.path.append('../venv/lib')

# set the current directory here
os.chdir(sys.path[0])

# load configuration file
a_yaml_file = open("config.yml")
parsed_yaml_file = yaml.load(a_yaml_file,
                             Loader=yaml.FullLoader)

# predictive or inverse dynamics?
predictive = False
if parsed_yaml_file['simulation'] == 'predictive':
    print('Running predictive dynamics')
    predictive = True

if predictive:
    # load kml file converted in txt from GPS visualiser
    GPS_data = pd.read_csv('../data/Route.txt', delimiter='\t', skiprows=3)
else:
    # load fit file
    fit_file = FitFile(parsed_yaml_file['fit_file']['path'] + parsed_yaml_file['fit_file']['name'] + '.fit')

# athlete data
m0 = parsed_yaml_file['athlete']['m']
h0 = parsed_yaml_file['athlete']['h']
W__max0 = 23.5 * m0
C__D0 = 4.45 * (m0**(-0.45))
A__f0 = 0.0293 * (h0 ** 0.725 * m0 ** 0.425)

# define a objective function for fitting exp data
def objective(x, a, b, c, d, e, f):
    return a * x ** 5 + b * x ** 4 + c * x ** 3 + d * x ** 2 + e * x + f

# import fit file data and create a pd dataframe
Watts = []
lat = []
lon = []
alt = []
dist = []
Speed = []
time_stamps = []
time = []

if not predictive:
    for record in fit_file.get_messages("record"):
        # Records can contain multiple pieces of data (ex: timestamp, latitude, longitude, etc)
        for data in record:
            # Print the name and value of the data (and the units if it has any)
            if data.name == 'power':
                Watts.append(data.value)
            if data.name == 'position_lat':
                lat.append(data.value/11930465)
            if data.name == 'position_long':
                lon.append(data.value/11930465)
            if data.name == 'altitude':
                alt.append(data.value)
            if data.name == 'distance':
                dist.append(data.value)
            if data.name == 'speed':
                Speed.append(data.value)
            if data.name == 'timestamp':
                time_stamps.append(data.value)

    for _time in time_stamps:
        time.append(_time.timestamp() - time_stamps[0].timestamp())
    time = np.array(time)

    # build the df
    end_index = min([len(time), len(dist), len(lon), len(lat), len(alt), len(Watts), len(Speed)])

    exp_data = pd.DataFrame()
    exp_data['time'] = time[0:end_index]
    exp_data['dist'] = dist[0:end_index]
    exp_data['lon'] = lon[0:end_index]
    exp_data['lat'] = lat[0:end_index]
    exp_data['alt'] = alt[0:end_index]
    exp_data['Watts'] = Watts[0:end_index]
    exp_data['Speed'] = Speed[0:end_index]
    exp_data = exp_data.drop_duplicates(['time', 'lat', 'lon'])
    exp_data = exp_data.drop(exp_data[exp_data.Speed < 3].index)
    exp_data = exp_data.reset_index()
    # start from the top of the hill? For Verona e.g.
    # first_point = (exp_data['alt']).idxmax()
    first_point = (exp_data['dist'] > 3000).idxmin()
    # after tot_length stop
    tot_length = 5000
    last_point = (exp_data['dist'] > ((exp_data['dist']).iloc[first_point] + tot_length)).idxmax()
    dist_tot_travelled = exp_data['dist'][first_point]
else:
    exp_data = pd.DataFrame()
    exp_data['lon'] = GPS_data['longitude']
    exp_data['lat'] = GPS_data['latitude']
    exp_data['alt'] = GPS_data['altitude (m)']
    first_point = 0
    last_point = 100
    # initial conditions for unknown variables
    dist_tot_travelled = np.asarray(0)
    v0 = 3

proportion_nodes_points = 1
n_points = 8
n_nodes = n_points * proportion_nodes_points
initial_distance = 0
time_tot_passed = 0
xCOM_current = 0
yCOM_current = 0
xEXP_current = 0
yEXP_current = 0
overlap = 1
# the safety margin is used to interpolate
# data and avoid bas interfacing problem between chunks
safety_margin = 2

# adjust GPS
rotMatrix = scipy.spatial.transform.Rotation.from_euler('z', 0, degrees=True).as_matrix()

# IC
alpha0 = 0
d_alpha0 = 0
n0 = 0
phi__dot0 = 0
phi0 = 0
delta__n0 = 0
W__n0 = 0
v__delta_n0 = 0
v__W_n0 = 0
adj_CdA0 = 1
adj_beta0 = 1

# post process
if (args.device == 'PC' or len(sys.argv) < 2) and parsed_yaml_file['plot']:
    # initialize plot on PC
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)

# set initial computational time
compt_time = time_count.time()

distance = list()
power = list()
speed = list()
alpha_sim = list()
n_sim = list()
CPU_time = list()
sim_time = list()
target_power = list()
target_speed = list()

for i in np.arange(first_point, last_point, n_points-overlap-1):

    lat0, lon0, alt0 = exp_data[['lat', 'lon', 'alt']].iloc[i]

    # wind estimation
    location = (lat0, lon0)
    if parsed_yaml_file['openweather']['settings']['active']:
        data_wind = owm.get_current(location, **parsed_yaml_file['openweather']['settings'])
    else:
        data_wind = dict()
        data_wind['wind'] = parsed_yaml_file['openweather']['default']

    Vw0__0 = data_wind['wind']['speed']
    wD__0 = data_wind['wind']['deg']/180*math.pi

    trajectory_points = np.empty([0, 3])
    geo_coords = exp_data[['lat', 'lon', 'alt']].values[i:i+n_points+safety_margin]
    trajectory_points = np.array(pm.geodetic2enu(geo_coords[:, 0],
                                                 geo_coords[:, 1],
                                                 geo_coords[:, 2],
                                                 lat0, lon0, alt0))

    # trajectory_points[1, :] = - trajectory_points[1, :]
    if not predictive:
        t = np.asarray(exp_data['time'].values[i:i+n_points])
        v_exp = np.asarray(exp_data['Speed'].values[i:i + n_points + safety_margin])
        watt_exp = np.asarray(exp_data['Watts'].values[i:i + n_points + safety_margin])

    enu = rotMatrix.dot(trajectory_points)

    x = enu[0, :]
    y = enu[1, :]
    z = enu[2, :]

    alt_exp = np.asarray(exp_data['alt'].values[i:i+n_points+safety_margin])
    dist_exp = np.sqrt((x[:-1] - x[1:]) ** 2 + (y[:-1] - y[1:]) ** 2)
    dist_exp_tot = np.sum(dist_exp)
    beta_exp = np.arctan(np.diff(alt_exp) / dist_exp)

    # popt, _ = curve_fit(objective, x, y)
    # a, b, c, d, e = popt
    x_steps = interp1d(np.arange(0, len(x), 1), x, fill_value="extrapolate", kind='cubic')
    x_new = x_steps(np.linspace(0, len(x)-1, n_nodes + (safety_margin * proportion_nodes_points)))
    y_steps = interp1d(np.arange(0, len(x), 1), y, fill_value="extrapolate", kind='cubic')
    y_new = y_steps(np.linspace(0, len(x)-1, n_nodes + (safety_margin * proportion_nodes_points)))

    nodes1 = np.asfortranarray([x_new[:], y_new[:]])
    curve1 = bezier.Curve.from_nodes(nodes1)

    s_bezier = np.linspace(0.0, 1.0, n_nodes + (safety_margin * proportion_nodes_points))
    xBexier = curve1.evaluate_multi(np.asfortranarray(s_bezier))[0, :]
    yBezier = curve1.evaluate_multi(np.asfortranarray(s_bezier))[1, :]

    dist_array = (xBexier[:-1] - xBexier[1:]) ** 2 + (yBezier[:-1] - yBezier[1:]) ** 2
    dist_tot = np.sum(np.sqrt(dist_array))
    s_inter = s_bezier * dist_tot

    theta_bezier = np.mod(np.arctan(np.diff(yBezier) / np.diff(xBexier)), math.pi)
    kappa_bezier = np.diff(theta_bezier) / np.diff(s_inter[:-1])

    if not predictive:
        # popt_v, _ = curve_fit(objective, np.concatenate(([0], np.cumsum(dist_exp))), v_exp)
        # vel = lambda x: popt_v[0] * x**5 + popt_v[1] * x**4 + popt_v[2] *x**3 + popt_v[3]*x**2 + popt_v[4] * x + popt_v[5]
        vel_steps = interp1d(np.concatenate(([0], np.cumsum(dist_exp))), v_exp, fill_value="extrapolate", kind='cubic')
        vel = lambda x: vel_steps(x)
        if i == first_point:
            v0 = v_exp[0]

        watt_steps = interp1d(np.concatenate(([0], np.cumsum(dist_exp))), watt_exp, fill_value="extrapolate", kind='linear')
        watt = lambda x: watt_steps(x)
    else:
        vel = lambda x: 0
        watt = lambda x: 0

    popt_beta, _ = curve_fit(objective, np.concatenate(([0], np.cumsum(dist_exp[:-1]))), beta_exp)
    beta = lambda x: popt_beta[0] * x ** 5 + popt_beta[1] * x ** 4 + popt_beta[2] * x ** 3 + popt_beta[3] * x**2 + popt_beta[4] * x + popt_beta[5]
    # beta_steps = interp1d(np.concatenate(([0], np.cumsum(dist_exp[:-1]))), beta_exp, fill_value="extrapolate", kind='cubic')
    # beta = lambda x: beta_steps(x)

    popt_k, _ = curve_fit(objective, s_inter[:-2], kappa_bezier)
    kappa = lambda x: popt_k[0] * x ** 5 + popt_k[1] * x ** 4 + popt_k[2] * x ** 3 + popt_k[3] * x**2 + popt_k[4] * x + popt_k[5]

    popt_theta, _ = curve_fit(objective, s_inter[:-1], theta_bezier)
    theta_fun = lambda x: popt_theta[0] * x ** 5 + popt_theta[1] * x ** 4 + popt_theta[2] * x ** 3 + popt_theta[3] * x**2 + popt_theta[4] * x + popt_theta[5]

    model = GEKKO()
    model.DIAGLEVEL = 0
    # maximal CPU time (s)
    model.MAX_TIME = n_points - overlap
    model.WEB = 0
    model.SEQUENTIAL = 0

    alpha = model.Var(lb=-0.1, ub=0.1, value=alpha0)
    s__dot = model.Var()
    v = model.Var(lb=3, value=v0)
    n = model.Var(lb=-4, ub=4, value=n0)
    phi__dot = model.Var(value=phi__dot0)
    phi = model.Var(value=phi0)
    delta__n = model.Var(value=delta__n0)
    W__n = model.Var(value=W__n0)
    s = model.Var(value=0)
    d_alpha = model.Var(value=d_alpha0)

    # Parameters
    delta__max = model.Param(value=0.52)
    L = model.Param(value=1.4)
    k = model.Param(value=0)
    d = model.Param(value=0.25)
    W__max = model.Param(value=W__max0)
    I__X = model.Param(value=1)
    h = model.Param(value=1)
    g = model.Param(value=9.81)
    C__rr = model.Param(value=0.005)
    C__D = model.Param(value=C__D0)
    A__f = model.Param(value=A__f0)
    rho = model.Param(value=1.23)
    m = model.Param(value=m0)
    theta = model.Param(value=0)
    mu__max = model.Param(value=1)
    Cb__0 = model.Param(value=0)
    Cb__1 = model.Param(value=0)
    Vw0 = model.Param(value=Vw0__0)
    wD = model.Param(value=wD__0)

    # time-invariant variables
    v_target = model.Param()
    beta_target = model.Param()

    # CONTROLS
    v__delta_n = model.MV(value=v__delta_n0, lb=-1, ub=1)
    v__delta_n.STATUS = 1
    v__W_n = model.MV(value=v__W_n0, lb=-1, ub=1)
    v__W_n.STATUS = 1
    adj_CdA = model.MV(value=adj_CdA0, lb=0.8, ub=1.2)
    adj_CdA.STATUS = 1
    adj_beta = model.MV(value=adj_beta0, lb=0.9, ub=1.1)
    adj_beta.STATUS = 1

    # ROAD & ENVIRONMENT
    Vw = lambda alpha, theta, Vw0, wD: Vw0 * model.cos(alpha + theta - wD)

    # math utilities
    neg_part = lambda x: x + (0.001 + 0.5 * (((-x) - 0.001) - model.sqrt(((-x) - 0.001) ** 2 + 0.001 ** 2)))
    pos_part = lambda x: x - (0.001 + 0.5 * (((x) - 0.001) - model.sqrt(((x) - 0.001) ** 2 + 0.001 ** 2)))

    # Constraints
    model.Equation(delta__n >= -1)
    model.Equation(delta__n <= 1)
    model.Equation(W__n >= -1)
    model.Equation(W__n <= 1)
    model.Equation(phi > -0.8)
    model.Equation(phi < 0.8)

    # set up optimiser
    model.options.IMODE = 6
    model.options.SOLVER = 3
    model.options.MAX_ITER = 1200

    # time -> space GEKKO notation
    model.time = s_inter[:-safety_margin]

    # target values of velocity
    v_target.value = vel(s_inter[:-safety_margin])
    # if target velocity os met THEN you can trust cubic approx of beta
    beta_target.value = beta(s_inter[:-safety_margin])

    # ICs: FREE the vars
    if i == first_point:
        model.free_initial(delta__n)
        model.free_initial(phi)
        model.free_initial(phi__dot)
        model.free_initial(W__n)
        if predictive:
            model.fix_initial(n, val=0)
        else:
            model.free_initial(n)
        model.fix_initial(s, val=0)
        model.fix_initial(v, val=v0)
    else:
        model.fix_initial(delta__n, val=delta__n0)
        model.fix_initial(phi, val=phi0)
        model.fix_initial(phi__dot, val=phi__dot0)
        model.fix_initial(W__n, val=W__n0)
        model.fix_initial(v, val=v0)
        if predictive:
            model.fix_initial(n, val=0)
        else:
            model.fix_initial(n, val=n0)
        model.fix_initial(s, val=0)
    model.free_initial(alpha)
    model.free_initial(d_alpha)
    #
    model.free_final(n)
    model.free_final(delta__n)
    model.free_final(phi)
    model.free_final(alpha)
    model.free_final(d_alpha)
    model.free_final(phi__dot)
    model.free_final(W__n)
    model.free_final(v)

    # FREE the controls
    if i == first_point:
        model.free_initial(v__delta_n)
        model.free_initial(v__W_n)
        model.free_initial(adj_CdA)
        model.free_initial(adj_beta)
    else:
        model.fix_initial(v__delta_n)
        model.fix_initial(v__W_n)
        model.fix_initial(adj_CdA)
        model.fix_initial(adj_beta)

    model.free_final(v__delta_n)
    model.free_final(v__W_n)
    model.free_final(adj_CdA)
    model.free_final(adj_beta)

    # Equations
    # 1) alpha heading angle
    model.Equation(alpha.dt() == -0.1e1 * (n * kappa(s) * delta__n * delta__max + model.cos(alpha) * kappa(
        s) * L - 0.1e1 * delta__max * delta__n) / L / model.cos(alpha))
    # 2) lateral displacement
    model.Equation(n.dt() == -0.1e1 * model.sin(alpha) / model.cos(alpha) * (-0.1e1 + kappa(s) * n))
    # 3) roll angle
    model.Equation(phi.dt() == -0.1e1 * phi__dot / model.cos(alpha) / v * (-0.1e1 + kappa(s) * n))
    # 4) norm steering angle
    model.Equation(delta__n.dt() == -0.1e1 * v__delta_n / model.cos(alpha) / v * (-0.1e1 + kappa(s) * n))
    # 5) norm power
    model.Equation(W__n.dt() == -0.1e1 * v__W_n / model.cos(alpha) / v * (-0.1e1 + kappa(s) * n))
    # 8) roll angle rate
    model.Equation(phi__dot.dt() == 0.5 / v ** 2 / L / (h ** 2 * m + I__X) * h / model.cos(alpha) * (
                C__D * A__f * Vw(alpha, theta, Vw0, wD) ** 2 * adj_CdA * v * n * kappa(s) * rho * delta__n * d * delta__max
                - 0.2e1 * C__D * A__f * Vw(alpha, theta, Vw0, wD) * adj_CdA * v ** 2 * n * kappa(
            s) * rho * delta__n * d * delta__max
                + C__D * A__f * adj_CdA * v ** 3 * n * kappa(s) * rho * delta__n * d * delta__max
                - 0.1e1 * C__D * A__f * Vw(alpha, theta, Vw0, wD) ** 2 * adj_CdA * v * rho * delta__n * d * delta__max
                + 0.2e1 * C__D * A__f * Vw(alpha, theta, Vw0, wD) * adj_CdA * v ** 2 * rho * delta__n * d * delta__max
                - 0.1e1 * C__D * A__f * adj_CdA * v ** 3 * rho * delta__n * d * delta__max
                - 0.2e1 * v * n * kappa(s) * neg_part(W__n) * g * m * mu__max * delta__n * d * delta__max
                + 0.2e1 * v * n * kappa(s) * model.cos(adj_beta * beta_target) * C__rr * g * m * delta__n * d * delta__max
                + 0.2e1 * v * n * kappa(s) * model.sin(adj_beta * beta_target) * g * m * delta__n * d * delta__max +
                0.2e1 * v * neg_part(W__n) * g * m * mu__max * delta__n * d * delta__max -
                0.2e1 * v * model.cos(adj_beta * beta_target) * g * m * C__rr * delta__n * d * delta__max
                - 0.2e1 * v ** 3 * delta__n * n * kappa(s) * m * delta__max -
                0.2e1 * v ** 2 * n * kappa(s) * Cb__1 * delta__n * d * delta__max -
                0.2e1 * v ** 2 * v__delta_n * n * kappa(s) * d * m * delta__max -
                0.2e1 * L * phi * v * n * kappa(s) * g * m -
                0.2e1 * v * model.sin(adj_beta * beta_target) * g * m * delta__n * d * delta__max -
                0.2e1 * v * kappa(s) * pos_part(W__n) * W__max * delta__n * d * delta__max +
                0.2e1 * v * n * kappa(s) * Cb__0 * delta__n * d * delta__max +
                0.2e1 * v ** 3 * delta__n * m * delta__max + 0.2e1 * v ** 2 * Cb__1 * delta__n * d * delta__max +
                0.2e1 * v ** 2 * v__delta_n * d * m * delta__max + 0.2e1 * L * phi * v * g * m +
                0.2e1 * pos_part(
            W__n) * W__max * delta__n * d * delta__max - 0.2e1 * v * Cb__0 * delta__n * d * delta__max))
    # 9) longitudinal speed
    model.Equation(v.dt() == 0.5000000000e0 * (C__D * A__f * Vw(alpha, theta, Vw0, wD) ** 2 * adj_CdA * v * n * kappa(s) * rho -
                                               0.2e1 * C__D * A__f * Vw(alpha, theta, Vw0, wD) * adj_CdA * v ** 2 * n * kappa(
                s) * rho +
                                               C__D * A__f * adj_CdA * v ** 3 * n * kappa(s) * rho -
                                               0.1e1 * C__D * A__f * Vw(alpha, theta, Vw0, wD) ** 2 * adj_CdA * v * rho +
                                               0.2e1 * C__D * A__f * Vw(alpha, theta, Vw0, wD) * adj_CdA * v ** 2 * rho -
                                               0.1e1 * C__D * A__f * adj_CdA * v ** 3 * rho -
                                               0.2e1 * v * n * kappa(s) * neg_part(W__n) * g * m * mu__max +
                                               0.2e1 * v * n * kappa(s) * model.cos(
                adj_beta * beta_target) * C__rr * g * m +
                                               0.2e1 * v * n * kappa(s) * model.sin(adj_beta * beta_target) * g * m +
                                               0.2e1 * v * neg_part(W__n) * g * m * mu__max -
                                               0.2e1 * v * model.cos(adj_beta * beta_target) * g * m * C__rr -
                                               0.2e1 * v ** 2 * n * kappa(s) * Cb__1 -
                                               0.2e1 * v * model.sin(adj_beta * beta_target) * g * m -
                                               0.2e1 * v * kappa(s) * pos_part(W__n) * W__max +
                                               0.2e1 * v * n * kappa(s) * Cb__0 +
                                               0.2e1 * v ** 2 * Cb__1 +
                                               0.2e1 * pos_part(W__n) * W__max -
                                               0.2e1 * v * Cb__0) / m / v ** 2 / model.cos(alpha))
    # 10) time
    # model.Equation(t_var.dt() == -0.1e1 / model.cos(alpha) / v * (-0.1e1 + kappa(s) * n))
    # 11) s
    model.Equation(s.dt() == 1)
    # 12) s
    model.Equation(alpha.dt() == d_alpha)

    # Objective function
    if not predictive:
        # Objective function COEFFICIENTS
        W__J1 = model.Param(value=1)
        W__J2 = model.Param(value=1)
        W__J3 = model.Param(value=0.01)
        W__J4 = model.Param(value=1)
        model.Obj((W__J1 * (v__delta_n ** 2) +
                   W__J2 * (v__W_n ** 2) +
                   W__J2 * 0.01 * (W__n ** 2) +
                   W__J3 * (n ** 2) +
                   W__J3 * (alpha ** 2) +
                   W__J4 * ((v - v_target) ** 2)))
    else:
        # Objective function COEFFICIENTS
        model.Equation(n >= -4)
        model.Equation(n <= 4)
        W__J1 = model.Param(value=1)
        W__J2 = model.Param(value=1)
        model.Obj((W__J1 * (v__delta_n ** 2) +
                   W__J2 * (v__W_n ** 2) +
                   W__J2 * 0.01 * (W__n ** 2)
                   ))

    # launch solver
    model.solve(disp=False, debug=0)

    # POST PROCESS
    xCOM = curve1.evaluate_multi(np.asfortranarray(np.asarray(s.value) / dist_tot))[0, :] - np.sin(
        theta_fun(np.asarray(s.value) / dist_tot) + np.asarray(alpha.value)) * np.asarray(n.value)
    yCOM = curve1.evaluate_multi(np.asfortranarray(np.asarray(s.value) / dist_tot))[1, :] + np.cos(
        theta_fun(np.asarray(s.value) / dist_tot) + np.asarray(alpha.value)) * np.asarray(n.value)

    # VISUAL INSPECTION
    if (args.device == 'PC' or len(sys.argv) < 2) and parsed_yaml_file['plot']:

        if not predictive:
            ax1.plot(dist_tot_travelled + s_inter[:-safety_margin], v_target.value, '-k')
            ax1.scatter(dist_tot_travelled + np.concatenate(([0], np.cumsum(dist_exp))), v_exp)
        ax1.plot(dist_tot_travelled + s.value, v.value, '-b')
        # ax1.plot(dist_tot_travelled + np.concatenate(([0], np.cumsum(dist_exp))), watt_exp/100, '-k')

        if not predictive:
            ax2.plot(dist_tot_travelled + np.concatenate(([0], np.cumsum(dist_exp))), watt_exp, '-k')
        ax2.plot(dist_tot_travelled + s.value, np.asarray(W__n.value) * np.asarray(W__max.value), '-b')

        ax3.plot(xCOM_current + xCOM, yCOM_current + yCOM, '-r')
        ax3.plot(xEXP_current + x, yEXP_current + y, '-k')
        ax3.axis('equal')

        ax4.plot(dist_tot_travelled + s_inter[:-safety_margin], beta(s_inter[:-safety_margin]), '-b')
        # ax4.plot(dist_tot_travelled + np.concatenate(([0], np.cumsum(dist_exp))), beta_exp, '-k')

    if args.device == 'mobile' and parsed_yaml_file['plot']:
        plotext.clp()
        plotext.clt()
        plotext.scatter(xCOM_current + xCOM, yCOM_current + yCOM, point_color = "tomato")
        plotext.scatter(xCOM_current + x, yCOM_current + y, point_color = "green")
        plotext.show()

    # UPDATE states and controls
    rewind_index = (overlap) * proportion_nodes_points - 2
    if rewind_index < 0:
        rewind_index = -rewind_index
    if rewind_index == 0:
        rewind_index = 1

    alpha0 = alpha.value[-rewind_index]
    d_alpha0 = d_alpha.value[-rewind_index]
    n0 = n.value[-rewind_index]
    phi__dot0 = phi__dot.value[-rewind_index]
    phi0 = phi.value[-rewind_index]
    delta__n0 = delta__n.value[-rewind_index]
    W__n0 = W__n.value[-rewind_index]
    if not predictive:
        v0 = v_exp[-overlap-1-safety_margin]
    else:
        v0 = v.value[-rewind_index]
    v__delta_n0 = v__delta_n.value[-rewind_index]
    v__W_n0 = v__W_n.value[-rewind_index]
    adj_CdA0 = adj_CdA.value[-rewind_index]
    adj_beta0 = adj_beta.value[-rewind_index]

    xCOM_current = xCOM[-rewind_index] + xCOM_current
    yCOM_current = yCOM[-rewind_index] + yCOM_current
    xEXP_current = x[-overlap-1-safety_margin] + xEXP_current
    yEXP_current = y[-overlap-1-safety_margin] + yEXP_current

    CPUtime_ = time_count.time() - compt_time
    print('Elapsed time:', CPUtime_, ' s')
    compt_time = time_count.time()

    distance.append((dist_tot_travelled + s.value).tolist())
    power.append((np.asarray(W__n.value) * np.asarray(W__max.value)).tolist())
    speed.append((np.asarray(v.value)).tolist())
    alpha_sim.append((np.asarray(alpha.value)).tolist())
    n_sim.append((np.asarray(n.value)).tolist())
    sim_time.append((time_tot_passed + np.asarray(s.value)/np.asarray(v.value)).tolist())
    if not predictive:
        target_power.append((watt(s.value)).tolist())
        target_speed.append((vel(s.value)).tolist())
    else:
        target_power.append(np.asarray(s.value).tolist())
        target_speed.append(np.asarray(s.value).tolist())
    CPU_time.append(CPUtime_)

    dist_tot_travelled = dist_tot_travelled + np.cumsum(dist_exp)[-overlap-1-safety_margin]
    time_tot_passed = time_tot_passed + np.asarray(s.value[-rewind_index])/np.asarray(v.value[-rewind_index])

df_results = pd.DataFrame()

df_results['time'] = np.concatenate(sim_time, axis=0)
df_results['speed'] = np.concatenate(speed, axis=0)
df_results['power'] = np.concatenate(power, axis=0)
df_results['alpha'] = np.concatenate(alpha_sim, axis=0)
df_results['n'] = np.concatenate(n_sim, axis=0)
df_results['target_power'] = np.concatenate(target_power, axis=0)
df_results['target_speed'] = np.concatenate(target_speed, axis=0)
df_results['abs_power'] = df_results['power']
df_results['abs_power'][df_results.abs_power < 0] = 0

# drop duplicates (due to overlapping)
df_results = df_results.drop_duplicates('time')
df_results = df_results[np.diff(df_results['time'], prepend=0) > 0]
# add row of zeros
df0 = pd.DataFrame([[0]*df_results.shape[1]], columns=df_results.columns)
df_results = df0.append(df_results, ignore_index=True)

# create time series
df_results['time'] = pd.to_datetime(df_results["time"], unit = 's')
df_results = df_results.set_index('time')

if args.device == 'PC' and parsed_yaml_file['plot']:
    fig, (ax1) = plt.subplots(1, 1)
    ax1.scatter(30, np.max(df_results.rolling('30s').abs_power.mean()), facecolor='blue')
    ax1.scatter(120, np.max(df_results.rolling('120s').abs_power.mean()), facecolor='blue')
    ax1.scatter(300, np.max(df_results.rolling('300s').abs_power.mean()), facecolor='blue')
    ax1.scatter(600, np.max(df_results.rolling('600s').abs_power.mean()), facecolor='blue')
    ax1.scatter(30, np.max(df_results.rolling('30s').target_power.mean()), facecolor='red')
    ax1.scatter(120, np.max(df_results.rolling('120s').target_power.mean()), facecolor='red')
    ax1.scatter(300, np.max(df_results.rolling('300s').target_power.mean()), facecolor='red')
    ax1.scatter(600, np.max(df_results.rolling('600s').target_power.mean()), facecolor='red')

# save a dict with the results
df_results = df_results.reset_index()
df_results['time'] = df_results['time'].astype(str)
dict_results = df_results.to_dict()
dict_results['CPU_time'] = CPU_time

# write json
with open('../output/' + parsed_yaml_file['fit_file']['name'] + '_results.json', 'w') as outfile:
    json.dump(dict_results, outfile)

EOF = 1
# EOF