def optimal_filter(t, y, my_lambda):
    import numpy as np
    # be robust
    for i in np.arange(1, len(t)-1):
        if t[i+1] == t[i]:
            t[i+1] = t[i] + 0.5

    h = 0.5 * np.concatenate([[t[1]-t[0]], t[2:] - t[0:-2], [t[-1] - t[-2]]])
    # robust
    m = np.median(h[h > 0])
    # Assign the median to the zero elements
    h[h == 0] = m

    dg = np.divide(my_lambda, h)
    # symmetric tri-diagonal system
    a = - dg[1:]
    b = np.diff(t) + dg[0:-1] + dg[1:]
    u = np.diff(y)
    # solution of the system
    n = len(u)

    for j in np.arange(0, n - 1):
        mu = a[j] / b[j]
        b[j + 1] = b[j + 1] - mu * a[j]
        u[j + 1] = u[j + 1] - mu * u[j]

    u[n-1] = u[n-1] / b[n-1]

    for j in np.arange(n-2, -1, -1):
        u[j] = (u[j] - a[j] * u[j + 1]) / b[j]

    # retrieving solution
    x = np.empty([len(y), ])
    x[0] = y[0] + my_lambda * u[0] / h[0]

    for i in np.arange(0, n):
        x[i + 1] = x[i] + (t[i + 1] - t[i]) * u[i]

    return x

def compute_kappa(x, y, n_nodes, margin_ahead, proportion_nodes_points):
    import bezier
    from scipy.interpolate import interp1d
    import numpy as np
    # interpolate the midline GPS points to have a better spatial separation
    # between nodes and avoid singularities
    x_steps = interp1d(np.arange(0, len(x), 1), x, fill_value="extrapolate", kind='next')
    x_new = x_steps(np.linspace(0, len(x)-1, n_nodes))
    x_new_F = optimal_filter(np.linspace(0, len(x) - 1, n_nodes), x_new, .05)
    y_steps = interp1d(np.arange(0, len(x), 1), y, fill_value="extrapolate", kind='next')
    y_new = y_steps(np.linspace(0, len(x)-1, n_nodes))
    y_new_F = optimal_filter(np.linspace(0, len(y) - 1, n_nodes), y_new, .05)

    # OC nodes are defined with the interpolation points
    nodes1 = np.asfortranarray([x_new_F, y_new_F])
    # a Bezier curve is defined for trajectory
    curve1 = bezier.Curve.from_nodes(nodes1)

    # curvilinear abscissa [0, 1] is defined and it will be used as independent coordinate in the OC problem
    s_bezier = np.linspace(0.0, 1.0, n_nodes)
    xBezier = curve1.evaluate_multi(np.asfortranarray(s_bezier))[0, :]
    yBezier = curve1.evaluate_multi(np.asfortranarray(s_bezier))[1, :]

    # compute the actual s [0, dist_tot]
    dist_array = np.diff(xBezier[:-margin_ahead*proportion_nodes_points])**2 + \
                 np.diff(yBezier[:-margin_ahead*proportion_nodes_points])**2
    dist_tot = np.sum(np.sqrt(dist_array))
    s_inter = s_bezier * dist_tot

    # compute theta nominal heading of the road (if statements are included to handle all the quadrants)
    theta_bezier = []
    for it_, (y_, x_) in enumerate(zip(yBezier[1:], xBezier[1:])):
        # theta_ = np.arctan(np.diff(yBezier)[it_] / np.diff(xBezier)[it_])
        # theta_bezier.append(theta_)
        # first quadrant
        if (y_ - yBezier[it_]) >= 0 and (x_ - xBezier[it_]) >= 0:
            theta_ = np.arctan(np.diff(yBezier)[it_]/np.diff(xBezier)[it_])
            theta_bezier.append(theta_)
        else:
            pass
        # second quadrant
        if (y_ - yBezier[it_]) >= 0 and (x_ - xBezier[it_]) < 0:
            theta_ = np.pi + np.arctan(np.diff(yBezier)[it_] / np.diff(xBezier)[it_])
            theta_bezier.append(theta_)
        else:
            pass
        # third quadrant
        if (y_ - yBezier[it_]) < 0 and (x_ - xBezier[it_]) < 0:
            theta_ = - np.pi + np.arctan(np.diff(yBezier)[it_] / np.diff(xBezier)[it_])
            theta_bezier.append(theta_)
        else:
            pass
        # fourth quadrant
        if (y_ - yBezier[it_]) < 0 and (x_ - xBezier[it_]) >= 0:
            theta_ = (np.arctan(np.diff(yBezier)[it_] / np.diff(xBezier)[it_]))
            theta_bezier.append(theta_)
        else:
            pass

    theta_bezier = np.asarray(theta_bezier)

    # compute road curvature kappa (1/R)
    kappa_bezier = np.diff(theta_bezier) / np.diff(s_inter[:-1])

    return kappa_bezier, theta_bezier, curve1, s_bezier, dist_tot, s_inter
