import json
from os import listdir
from os.path import isfile, join
import plotly.express as px
import pandas as pd
import numpy as np
import plotly.graph_objects as go
import argparse
import yaml
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser()
parser.add_argument("--device", help="running fromm PC or mobile")
args = parser.parse_args()

# load configuration file
a_yaml_file = open("config.yml")
parsed_yaml_file = yaml.load(a_yaml_file,
                             Loader=yaml.FullLoader)

file_path = '../output/'
# onlyfiles = [f for f in listdir(file_path) if isfile(join(file_path, f))]
onlyfiles = ['giro_2018_results.json']

CPU_times = {}
mean_power_error = []
mean_vel_error = []
mean_alpha_error = []
mean_n_error = []
for i in onlyfiles:
    try:
        with open(file_path + i) as f:
            data = json.load(f)
            CPU_times[i] = data['CPU_time']
            mean_power_error.append(np.mean(np.nan_to_num(np.asarray(list(data['target_power'].values()))))
                  - np.mean(np.abs(np.asarray(list(data['power'].values())))))
            mean_vel_error.append(np.mean(np.nan_to_num(np.asarray(list(data['target_speed'].values()))))
                                    - np.mean(np.abs(np.asarray(list(data['speed'].values())))))
            mean_alpha_error.append(np.mean(np.abs(np.asarray(list(data['alpha'].values())))))
            mean_n_error.append(np.mean(np.abs(np.asarray(list(data['n'].values())))))
            # time series (single file)
            df = pd.DataFrame.from_dict(data['power'], orient='index')
            df.columns = ['power']
            df['abs_power'] = df['power']
            df['abs_power'][df.abs_power < 0] = 0
            df['target_power'] = pd.DataFrame.from_dict(data['target_power'], orient='index')
            df['target_speed'] = pd.DataFrame.from_dict(data['target_speed'], orient='index')
            df['speed'] = pd.DataFrame.from_dict(data['speed'], orient='index')
            df['timestamp'] = pd.DataFrame.from_dict(data['time'], orient='index')
            time_sec = float(df['timestamp'].iloc[0].split(':')[-1])
            for time_ in df['timestamp'].iloc[1:]:
                time_sec = np.vstack((time_sec, float(time_.split(':')[-2]) * 60 + float(time_.split(':')[-1])))
            df['time'] = time_sec
            # drop duplicates (due to overlapping)
            df = df.drop_duplicates('time')
            df = df[np.diff(df['time'], prepend=0) > 0]
            # add row of zeros
            df0 = pd.DataFrame([[0] * df.shape[1]], columns=df.columns)
            df = df0.append(df, ignore_index=True)

            # create time series
            df['time'] = pd.to_datetime(df["time"], unit='s')
            df = df.set_index('time')
            fig, (ax1) = plt.subplots(1, 1)
            ax1.scatter(30, np.max(df.rolling('30s').abs_power.mean()), facecolor='blue')
            ax1.scatter(120, np.max(df.rolling('120s').abs_power.mean()), facecolor='blue')
            ax1.scatter(300, np.max(df.rolling('300s').abs_power.mean()), facecolor='blue')
            ax1.scatter(600, np.max(df.rolling('600s').abs_power.mean()), facecolor='blue')
            ax1.scatter(30, np.max(df.rolling('30s').target_power.mean()), facecolor='red')
            ax1.scatter(120, np.max(df.rolling('120s').target_power.mean()), facecolor='red')
            ax1.scatter(300, np.max(df.rolling('300s').target_power.mean()), facecolor='red')
            ax1.scatter(600, np.max(df.rolling('600s').target_power.mean()), facecolor='red')
            dummy =1
    except:
        pass

df_CPU = pd.DataFrame.from_dict(CPU_times, orient='index')
df_CPU = df_CPU.transpose()

avg_CPU_time = []
for col in df_CPU.columns:
    avg_CPU_time.append(np.mean(df_CPU[str(col)]))


if args.device == 'PC' and parsed_yaml_file['plot']:
    template = 'plotly_white'

    # violin plot all the CPU time values
    fig = go.Figure()
    counter = 1
    for col in df_CPU.columns:
        fig.add_trace(go.Violin(y=df_CPU[str(col)], name=str(counter)))
        counter += 1
    fig.update_traces(meanline_visible=True, points='all', jitter=0.05)
    fig.update_layout(
        showlegend=False,
        legend=dict(
            yanchor="top",
            y=0.99,
            xanchor="left",
            x=0.01
        ),
        height=600,
        width=600,
        template=template,
        title='CPU time single loop',
        xaxis_title='Races',
        yaxis_title='CPU time (s)',
        yaxis_range=[4, 10]
    )
    fig.show()

    fig = go.Figure()
    fig.add_trace(go.Scatter(x=df.time, y=df.target_power, fill='tonexty', mode='none', name="Experimental"))
    fig.add_trace(go.Scatter(x=df.time, y=df.power, fill='tozeroy', mode='none', name="Simulated"))
    fig.update_layout(
        showlegend=True,
        legend=dict(
            yanchor="top",
            y=0.99,
            xanchor="left",
            x=0.01
        ),
        template=template,
        title='Estimated and experimental power-output',
        xaxis_title='Seconds (s)',
        yaxis_title='Power-output (W)'
    )
    fig.show()

    fig = go.Figure()
    fig.add_trace(go.Scatter(x=df.time, y=df.target_speed, name="Experimental"))
    fig.add_trace(go.Scatter(x=df.time, y=df.speed, name="Simulated"))
    fig.update_layout(
        showlegend=True,
        legend=dict(
            yanchor="top",
            y=0.99,
            xanchor="left",
            x=0.01
        ),
        template=template,
        title='Estimated and experimental power-output',
        xaxis_title='Seconds (s)',
        yaxis_title='Power-output (W)'
    )
    fig.show()

    dummy=1