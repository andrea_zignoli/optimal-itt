clc
clear all

Figure_Settings;

D = importdata('../output/results.csv');

%% 

zeta            = D.data(:,2);
OC_speed        = D.data(:,4);
speed           = D.data(:,3);
ax              = D.data(:,5);
ay              = D.data(:,6);
kappa_bike      = D.data(:,7); % OC
kappa_midline   = D.data(:,8);
kappa_RT        = D.data(:,9);
segment         = D.data(:,10);
pitch           = D.data(:,11);
index           = 1:length(zeta);
change_points   = index(diff(segment)>1);

zeta_0 = zeta(1);
zeta_f = zeta(end);
% zeta_0 = 600;
% zeta_f = 1200;

%% Compute adherence ellipse area

C = cov(ax,ay);
e = eig(C);
ellipse_area = sqrt(e(1) * e(2)) * pi * chi2inv(0.95,2);

%% figure

figure('Position', [100 100 1200, 400])
%
subplot(2,1,1)
hold on
for i = 1:length(change_points)
    xline(zeta(change_points(i)), 'color', colSlateGray);
end

plot(zeta(1:change_points(1)), OC_speed(1:change_points(1)), 'color', colBlue)
plot(zeta(1:change_points(1)), speed(1:change_points(1)), 'color', [0 0 0])

for i = 2:length(change_points)
   
   if bitget(i,1)
       plot(zeta(change_points(i-1)+1:change_points(i)), OC_speed(change_points(i-1)+1:change_points(i)), 'color', colBlue)
       plot(zeta(change_points(i-1)+1:change_points(i)), speed(change_points(i-1)+1:change_points(i)), 'color', [0 0 0])
   else
       plot(zeta(change_points(i-1)+1:change_points(i)), OC_speed(change_points(i-1)+1:change_points(i)), 'color', colBlue)
       plot(zeta(change_points(i-1)+1:change_points(i)), speed(change_points(i-1)+1:change_points(i)), 'color', [0 0 0])
   end
   
end

plot(zeta(change_points(end)+1:end), OC_speed(change_points(end)+1:end), 'color', colBlue)
plot(zeta(change_points(end)+1:end), speed(change_points(end)+1:end), 'color', [0,0,0])

scatter(zeta(diff([0; segment])>1), OC_speed(diff([0; segment])>1), 20, ...
    'markerfacecolor', [1, 1, 1], 'markerfacealpha', 0.5, 'markeredgecolor', [0 0 0 ])
% plot(zeta, OC_speed,'linewidth',1, 'color', colOrangeRed)

% fill_between(zeta, OC_speed, speed, speed<OC_speed, colForestGreen)
% fill_between(zeta, OC_speed, speed, speed>OC_speed, colOrangeRed)
ylabel('Speed (km/h)')
box on
xlim([zeta_0 zeta_f])
grid minor
title 'Curve warning system'
ax1 = gca();
ax1.XTickLabel = [];
% 
subplot(2,1,2)
hold on
for i = 1:length(change_points)
    segment_lines(i) = xline(zeta(change_points(i)), 'color', colSlateGray);
    segment_lines(i).Annotation.LegendInformation.IconDisplayStyle = 'off'; 
end

plot(zeta(1:change_points(1)), kappa_bike(1:change_points(1)), 'color', colBlue)
plot(zeta(1:change_points(1)), kappa_midline(1:change_points(1)), 'color', colRed)
plot(zeta(1:change_points(1)), kappa_RT(1:change_points(1)), 'color', [0 0 0])

for i = 2:length(change_points)
   
   if bitget(i,1)
       plot(zeta(change_points(i-1)+1:change_points(i)), kappa_bike(change_points(i-1)+1:change_points(i)), 'color', colBlue);
       plot(zeta(change_points(i-1)+1:change_points(i)), kappa_RT(change_points(i-1)+1:change_points(i)), 'color', [0 0 0])
       plot(zeta(change_points(i-1)+1:change_points(i)), kappa_midline(change_points(i-1)+1:change_points(i)), 'color', colRed)
   else
       plot(zeta(change_points(i-1)+1:change_points(i)), kappa_bike(change_points(i-1)+1:change_points(i)), 'color', colBlue)
       plot(zeta(change_points(i-1)+1:change_points(i)), kappa_RT(change_points(i-1)+1:change_points(i)), 'color', [0 0 0])
       plot(zeta(change_points(i-1)+1:change_points(i)), kappa_midline(change_points(i-1)+1:change_points(i)), 'color', colRed)
   end
   
end

plot(zeta(change_points(end)+1:end), kappa_bike(change_points(end)+1:end), 'color', colBlue)
plot(zeta(change_points(end)+1:end), kappa_RT(change_points(end)+1:end), 'color', [0,0,0])
plot(zeta(change_points(end)+1:end), kappa_midline(change_points(end)+1:end), 'color', colRed)

scatter(zeta(diff([0; segment])>1), kappa_bike(diff([0; segment])>1), 20, ...
    'markerfacecolor', [1, 1, 1], 'markerfacealpha', 0.5, 'markeredgecolor', [0 0 0 ])

ylabel('Curvature (1/m)')
% legend('Experimental','Optimal','Midline')
box on
xlim([zeta_0 zeta_f])
grid minor
xlabel 'Distance (m)'
ax2 = gca();
ax2.Position;
ax2.Position(2) = ax2.Position(2) + 0.05;

%% 
figure('Position', [100 100 600, 400])
hold on
grid on
plot_ellipse(1,     1,  0,0,0,colRed)
plot_ellipse(0.8,   0.8,0,0,0,colOrange)
plot_ellipse(0.7,   0.7,0,0,0,colGreen)
hold on
scatter(-ay, ax, 30, ...
    'markeredgecolor', colDodgerBlue, ...
    'markerfacecolor', colSkyBlue, 'markeredgealpha', 0.75, ...
    'markerfacealpha', 0.25)
axis equal
ylabel('Lon acc (g)')
xlabel('Lat acc (g)')
xlim([-1 1])
ylim([-1 1])
box off
my_ax = gca;
my_ax.XAxisLocation = 'origin';
my_ax.YAxisLocation = 'origin';
set(gca, 'color', 'none');
set(gcf, 'color', 'none');
export_fig('gg_plot', '-dpng', '-transparent', '-r300');

